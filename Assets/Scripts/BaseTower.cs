﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseTower : MonoBehaviour {

    public Vector3 projectileOrigin;
    public float range;
    public float fireRate;
    public float fireRateMax;
    public int damageMin;
    public int damageMax;
    private int damage;
    public int[] price;
    public string[] towerName;

    public int upgradeLevel;
    public int[] upgradeDamage;
    public int[] upgradeRange;
    public float[] upgradeSpeed;

    public GameObject projectilePrefab;

    void Awake () {
        projectileOrigin = transform.Find ("ProjectileOrigin").position;
        upgradeLevel = 0;
    }

    void Update () {

        fireRate -= Time.deltaTime;
        if (fireRate <= 0f) {
            fireRate = fireRateMax;

            BaseEnemy enemy = GetClosestEnemy ();

            if (enemy != null) {
                damage = Random.Range (damageMin - 1, damageMax + 1);
                if (this.gameObject.tag == "Arrow") {
                    if (enemy.enemyType == "HEAVY") {
                        damage = damage * 75 / 100;
                    } else if (enemy.enemyType == "MEDIUM") {
                        damage = damage * 125 / 100;
                    }
                }
                if (this.gameObject.tag == "Magic") {
                    if (enemy.enemyType == "MEDIUM") {
                        damage = damage * 75 / 100;
                    } else if (enemy.enemyType == "LIGHT") {
                        damage = damage * 125 / 100;
                    }
                }
                if (this.gameObject.tag == "Siege") {
                    if (enemy.enemyType == "LIGHT") {
                        damage = damage * 75 / 100;
                    } else if (enemy.enemyType == "HEAVY") {
                        damage = damage * 125 / 100;
                    }
                }

                BaseProjectile.Create (projectilePrefab, projectileOrigin, enemy, damage);
            }
        }
    }

    private BaseEnemy GetClosestEnemy () {
        return BaseEnemy.GetClosestEnemy (transform.position, range);
    }

    public float GetRange () {
        return range;
    }

    public void Upgrade () {
        if (upgradeLevel == 1) {
            damageMax += upgradeDamage[0];
            damageMin += upgradeDamage[0];
            range += upgradeRange[0];
            fireRateMax -= upgradeSpeed[0];
        }
        if (upgradeLevel == 2) {
            damageMax += upgradeDamage[1];
            damageMin += upgradeDamage[1];
            range += upgradeRange[1];
            fireRateMax -= upgradeSpeed[1];
        }
    }

    public void OnMouseDown () {
        if (!SpawnManager.SM.victory) {
            BuildManager.BM.upgradeMenuOpen = true;
            BuildManager.BM.upgradePanel.SetActive (true);
            BuildManager.BM.buildPanel.SetActive (false);
            BuildManager.BM.helpMenu.SetActive (false);
            BuildManager.BM.pauseMenu.SetActive (false);
            BuildManager.BM.curSelected = this.gameObject;
        }
    }
}
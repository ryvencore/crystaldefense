﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HPManager {
    public event EventHandler OnHealthChanged;
    public event EventHandler OnHealthMaxChanged;
    public event EventHandler OnDamaged;
    public event EventHandler OnDead;
    public int healthMax;
    public int health;

    public HPManager (int healthMax) {
        this.healthMax = healthMax;
        health = healthMax;
    }

    public int GetHealth () {
        return health;
    }

    public int GetHealthMax () {
        return healthMax;
    }

    public void Damage (int amount) {
        health -= amount;
        if (health < 0) {
            health = 0;
        }
        OnHealthChanged?.Invoke (this, EventArgs.Empty);
        OnDamaged?.Invoke (this, EventArgs.Empty);

        if (health <= 0) {
            Die ();
        }
    }

    public void Die () {
        OnDead?.Invoke (this, EventArgs.Empty);
    }

    public bool IsDead () {
        return health <= 0;
    }

    public void SetHealthMax (int healthMax, bool fullHealth) {
        this.healthMax = healthMax;
        if (fullHealth) health = healthMax;
        OnHealthMaxChanged?.Invoke (this, EventArgs.Empty);
        OnHealthChanged?.Invoke (this, EventArgs.Empty);
    }
}
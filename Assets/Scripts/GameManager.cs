﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public static GameManager GM;
    public int gold;
    public int round;
    public int crystalLives;
    public Text livesHPText;

    public void Awake () {
        GM = this;
        BaseEnemy.enemyList = new List<BaseEnemy> ();
    }

    public void Update () {
        if (crystalLives <= 0) {
            crystalLives = 0;
            SpawnManager.SM.Defeat ();
        }
        livesHPText.text = "Lives: " + crystalLives;

    }

    public void normalSpeed () {
        Time.timeScale = 1f;
    }

    public void doubleSpeed () {
        Time.timeScale = 2f;
    }

}
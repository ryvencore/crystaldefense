﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuildManager : MonoBehaviour {

    public static BuildManager BM;
    public bool buildMenuOpen;
    public bool upgradeMenuOpen;
    public GameObject buildPanel;
    public GameObject upgradePanel;
    public GameObject curSelected;
    public GameObject curPreview;
    public GameObject[] towerPrefab;
    public GameObject[] towerPreview;
    public GameObject[] buildButtons;
    public GameObject upgradeButton;
    public TextMeshProUGUI upgradeText;
    public TextMeshProUGUI baseText;
    public TextMeshProUGUI upgradeName;
    public TextMeshProUGUI baseName;
    public GameObject floatingText;
    private Vector3 floatingTextOffset = new Vector3 (0, 30f, 0);
    public GameObject pauseMenu;
    public GameObject helpMenu;
    bool open;

    void Awake () {
        buildPanel.SetActive (false);
        upgradePanel.SetActive (false);
        pauseMenu.SetActive (false);
        helpMenu.SetActive (false);
        BM = this;
        curSelected = null;
        curPreview = null;
    }

    void Update () {
        if (upgradePanel.activeSelf)
            UpdateUpgradeInfo ();

        if (Input.GetKeyDown (KeyCode.Escape)) {
            if (!open) {
                pauseMenu.SetActive (true);
                buildPanel.SetActive (false);
                upgradePanel.SetActive (false);
                helpMenu.SetActive (false);
            } else {
                pauseMenu.SetActive (false);

            }
            open = !open;
        }
    }

    public void CloseBuildMenu () {
        buildPanel.SetActive (false);
        curSelected = null;
        buildMenuOpen = false;
    }
    public void CloseUpgradeMenu () {
        upgradePanel.SetActive (false);
        curSelected = null;
        upgradeMenuOpen = false;
    }

    public void SellTower () {

        if (curSelected.GetComponent<BaseTower> ().upgradeLevel == 0) GameManager.GM.gold += (curSelected.GetComponent<BaseTower> ().price[0] * 75 / 100);
        else if (curSelected.GetComponent<BaseTower> ().upgradeLevel == 1) GameManager.GM.gold += ((curSelected.GetComponent<BaseTower> ().price[1] + curSelected.GetComponent<BaseTower> ().price[0]) * 75 / 100);
        else if (curSelected.GetComponent<BaseTower> ().upgradeLevel == 2) GameManager.GM.gold += ((curSelected.GetComponent<BaseTower> ().price[1] + curSelected.GetComponent<BaseTower> ().price[0] + curSelected.GetComponent<BaseTower> ().price[2]) * 75 / 100);
        upgradePanel.SetActive (false);
        upgradeMenuOpen = false;
        curSelected.transform.parent.GetComponentInParent<BuildSpot> ().spotTaken = false;
        Destroy (curSelected.gameObject);
    }

    public void NotEnoughGoldText (Vector3 loc, Transform parent) {
        GameObject clone = Instantiate (floatingText, loc, Quaternion.identity);
        clone.transform.SetParent (parent);
        Destroy (clone, 1f);
    }

    public void BuildOne () {
        if (GameManager.GM.gold >= towerPrefab[0].GetComponent<BaseTower> ().price[0]) {
            GameManager.GM.gold -= towerPrefab[0].GetComponent<BaseTower> ().price[0];
            Destroy (curPreview.gameObject);
            curPreview = null;
            GameObject tower = Instantiate (towerPrefab[0], curSelected.transform.position, Quaternion.identity);
            tower.transform.parent = curSelected.transform;
            curSelected.GetComponent<BuildSpot> ().spotTaken = true;
            CloseBuildMenu ();
        } else NotEnoughGoldText (buildButtons[0].transform.position + floatingTextOffset, buildPanel.transform);
    }
    public void BuildTwo () {
        if (GameManager.GM.gold >= towerPrefab[1].GetComponent<BaseTower> ().price[0]) {
            GameManager.GM.gold -= towerPrefab[1].GetComponent<BaseTower> ().price[0];
            Destroy (curPreview.gameObject);
            curPreview = null;
            GameObject tower = Instantiate (towerPrefab[1], curSelected.transform.position, Quaternion.identity);
            tower.transform.parent = curSelected.transform;
            curSelected.GetComponent<BuildSpot> ().spotTaken = true;
            CloseBuildMenu ();
        } else NotEnoughGoldText (buildButtons[1].transform.position + floatingTextOffset, buildPanel.transform);
    }
    public void BuildThree () {
        if (GameManager.GM.gold >= towerPrefab[2].GetComponent<BaseTower> ().price[0]) {
            GameManager.GM.gold -= towerPrefab[2].GetComponent<BaseTower> ().price[0];
            Destroy (curPreview.gameObject);
            curPreview = null;
            GameObject tower = Instantiate (towerPrefab[2], curSelected.transform.position, Quaternion.identity);
            tower.transform.parent = curSelected.transform;
            curSelected.GetComponent<BuildSpot> ().spotTaken = true;
            CloseBuildMenu ();
        } else NotEnoughGoldText (buildButtons[2].transform.position + floatingTextOffset, buildPanel.transform);
    }

    public void UpdateUpgradeInfo () {
        if (curSelected.gameObject.tag == "Arrow") {
            BaseTower arrow = curSelected.GetComponent<BaseTower> ();
            if (arrow.upgradeLevel == 0) {
                baseName.text = arrow.towerName[0];
                baseText.text = "Damage: " + arrow.damageMin + "-" + arrow.damageMax + "\nRange: " + arrow.range + "\nRate of Fire: " + arrow.fireRateMax;
                upgradeName.text = "Upgrade to " + arrow.towerName[1];
                upgradeText.text = "Damage: +" + arrow.upgradeDamage[0] + "\nRange: +" + arrow.upgradeRange[0] + "\nRate of Fire: -" + arrow.upgradeSpeed[0] + "\nPrice: " + arrow.price[1] + "G";
                upgradeButton.GetComponent<Button> ().interactable = true;
            } else if (arrow.upgradeLevel == 1) {
                baseName.text = arrow.towerName[1];
                baseText.text = "Damage: " + arrow.damageMin + "-" + arrow.damageMax + "\nRange: " + arrow.range + "\nRate of Fire: " + arrow.fireRateMax;
                upgradeName.text = "Upgrade to " + arrow.towerName[2];
                upgradeText.text = "Damage: +" + arrow.upgradeDamage[1] + "\nRange: +" + arrow.upgradeRange[1] + "\nRate of Fire: -" + arrow.upgradeSpeed[1] + "\nPrice: " + arrow.price[2] + "G";
                upgradeButton.GetComponent<Button> ().interactable = true;
            } else if (arrow.upgradeLevel == 2) {
                baseName.text = arrow.towerName[2];
                baseText.text = "Damage: " + arrow.damageMin + "-" + arrow.damageMax + "\nRange: " + arrow.range + "\nRate of Fire: " + arrow.fireRateMax;
                upgradeName.text = "MAXED OUT";
                upgradeText.text = "";
                upgradeButton.GetComponent<Button> ().interactable = false;
            }
        }
        if (curSelected.gameObject.tag == "Magic") {
            BaseTower magic = curSelected.GetComponent<BaseTower> ();
            if (magic.upgradeLevel == 0) {
                baseName.text = magic.towerName[0];
                baseText.text = "Damage: " + magic.damageMin + "-" + magic.damageMax + "\nRange: " + magic.range + "\nRate of Fire: " + magic.fireRateMax;
                upgradeName.text = "Upgrade to " + magic.towerName[1];
                upgradeText.text = "Damage: +" + magic.upgradeDamage[0] + "\nRange: +" + magic.upgradeRange[0] + "\nRate of Fire: -" + magic.upgradeSpeed[0] + "\nPrice: " + magic.price[1] + "G";
                upgradeButton.GetComponent<Button> ().interactable = true;
            } else if (magic.upgradeLevel == 1) {
                baseName.text = magic.towerName[1];
                baseText.text = "Damage: " + magic.damageMin + "-" + magic.damageMax + "\nRange: " + magic.range + "\nRate of Fire: " + magic.fireRateMax;
                upgradeName.text = "Upgrade to " + magic.towerName[2];
                upgradeText.text = "Damage: +" + magic.upgradeDamage[1] + "\nRange: +" + magic.upgradeRange[1] + "\nRate of Fire: -" + magic.upgradeSpeed[1] + "\nPrice: " + magic.price[2] + "G";
                upgradeButton.GetComponent<Button> ().interactable = true;
            } else if (magic.upgradeLevel == 2) {
                baseName.text = magic.towerName[2];
                baseText.text = "Damage: " + magic.damageMin + "-" + magic.damageMax + "\nRange: " + magic.range + "\nRate of Fire: " + magic.fireRateMax;
                upgradeName.text = "MAXED OUT";
                upgradeText.text = "";
                upgradeButton.GetComponent<Button> ().interactable = false;
            }
        }
        if (curSelected.gameObject.tag == "Siege") {
            BaseTower siege = curSelected.GetComponent<BaseTower> ();
            if (siege.upgradeLevel == 0) {
                baseName.text = siege.towerName[0];
                baseText.text = "Damage: " + siege.damageMin + "-" + siege.damageMax + "\nRange: " + siege.range + "\nRate of Fire: " + siege.fireRateMax;
                upgradeName.text = "Upgrade to " + siege.towerName[1];
                upgradeText.text = "Damage: +" + siege.upgradeDamage[0] + "\nRange: +" + siege.upgradeRange[0] + "\nRate of Fire: -" + siege.upgradeSpeed[0] + "\nPrice: " + siege.price[1] + "G";
                upgradeButton.GetComponent<Button> ().interactable = true;
            } else if (siege.upgradeLevel == 1) {
                baseName.text = siege.towerName[1];
                baseText.text = "Damage: " + siege.damageMin + "-" + siege.damageMax + "\nRange: " + siege.range + "\nRate of Fire: " + siege.fireRateMax;
                upgradeName.text = "Upgrade to " + siege.towerName[2];
                upgradeText.text = "Damage: +" + siege.upgradeDamage[1] + "\nRange: +" + siege.upgradeRange[1] + "\nRate of Fire: -" + siege.upgradeSpeed[1] + "\nPrice: " + siege.price[2] + "G";
                upgradeButton.GetComponent<Button> ().interactable = true;
            } else if (siege.upgradeLevel == 2) {
                baseName.text = siege.towerName[2];
                baseText.text = "Damage: " + siege.damageMin + "-" + siege.damageMax + "\nRange: " + siege.range + "\nRate of Fire: " + siege.fireRateMax;
                upgradeName.text = "MAXED OUT";
                upgradeText.text = "";
                upgradeButton.GetComponent<Button> ().interactable = false;
            }
        }
    }

    public void UpgradeTower () {
        if (curSelected.gameObject.tag == "Arrow") {
            BaseTower arrow = curSelected.GetComponent<BaseTower> ();
            if (arrow.upgradeLevel == 0) {
                if (GameManager.GM.gold >= arrow.price[1]) {
                    GameManager.GM.gold -= arrow.price[1];
                    arrow.upgradeLevel++;
                    arrow.Upgrade ();
                    arrow.transform.Find ("Upgrade1").gameObject.SetActive (true);
                    upgradePanel.SetActive (false);
                    upgradeMenuOpen = false;
                    curPreview = null;
                } else NotEnoughGoldText (upgradeButton.transform.position + floatingTextOffset, upgradePanel.transform);
            } else if (arrow.upgradeLevel == 1) {
                if (GameManager.GM.gold >= arrow.price[2]) {
                    GameManager.GM.gold -= arrow.price[2];
                    arrow.upgradeLevel++;
                    arrow.transform.Find ("Upgrade2").gameObject.SetActive (true);
                    arrow.Upgrade ();
                    upgradePanel.SetActive (false);
                    upgradeMenuOpen = false;
                    curPreview = null;
                } else NotEnoughGoldText (upgradeButton.transform.position + floatingTextOffset, upgradePanel.transform);
            }
        }
        if (curSelected.gameObject.tag == "Magic") {
            BaseTower magic = curSelected.GetComponent<BaseTower> ();
            if (magic.upgradeLevel == 0) {
                if (GameManager.GM.gold >= magic.price[1]) {
                    GameManager.GM.gold -= magic.price[1];
                    magic.upgradeLevel++;
                    magic.Upgrade ();
                    magic.transform.Find ("Upgrade1").gameObject.SetActive (true);
                    upgradePanel.SetActive (false);
                    upgradeMenuOpen = false;
                    curPreview = null;
                } else NotEnoughGoldText (upgradeButton.transform.position + floatingTextOffset, upgradePanel.transform);
            } else if (magic.upgradeLevel == 1) {
                if (GameManager.GM.gold >= magic.price[2]) {
                    GameManager.GM.gold -= magic.price[2];
                    magic.upgradeLevel++;
                    magic.Upgrade ();
                    magic.transform.Find ("Upgrade2").gameObject.SetActive (true);
                    upgradePanel.SetActive (false);
                    upgradeMenuOpen = false;
                    curPreview = null;
                } else NotEnoughGoldText (upgradeButton.transform.position + floatingTextOffset, upgradePanel.transform);
            }
        }
        if (curSelected.gameObject.tag == "Siege") {
            BaseTower siege = curSelected.GetComponent<BaseTower> ();
            if (siege.upgradeLevel == 0) {
                if (GameManager.GM.gold >= siege.price[1]) {
                    GameManager.GM.gold -= siege.price[1];
                    siege.upgradeLevel++;
                    siege.Upgrade ();
                    siege.transform.Find ("Upgrade1").gameObject.SetActive (true);
                    upgradePanel.SetActive (false);
                    upgradeMenuOpen = false;
                    curPreview = null;
                } else NotEnoughGoldText (upgradeButton.transform.position + floatingTextOffset, upgradePanel.transform);
            } else if (siege.upgradeLevel == 1) {
                if (GameManager.GM.gold >= siege.price[2]) {
                    GameManager.GM.gold -= siege.price[2];
                    siege.upgradeLevel++;
                    siege.Upgrade ();
                    siege.transform.Find ("Upgrade2").gameObject.SetActive (true);
                    upgradePanel.SetActive (false);
                    upgradeMenuOpen = false;
                    curPreview = null;
                } else NotEnoughGoldText (upgradeButton.transform.position + floatingTextOffset, upgradePanel.transform);
            }
        }
    }

    public void OnMouseOver (string towerName) {
        if (buildPanel.activeSelf) {
            if (towerName == "One") {
                curPreview = Instantiate (towerPreview[0], curSelected.transform.position, Quaternion.identity);
            } else if (towerName == "Two") {
                curPreview = Instantiate (towerPreview[1], curSelected.transform.position, Quaternion.identity);
            } else if (towerName == "Three") {
                curPreview = Instantiate (towerPreview[2], curSelected.transform.position, Quaternion.identity);
            }
        }
    }
    public void OnMouseExit (string towerName) {
        if (buildPanel.activeSelf) {
            if (towerName == "One") {
                Destroy (curPreview.gameObject);
                curPreview = null;
            } else if (towerName == "Two") {
                Destroy (curPreview.gameObject);
                curPreview = null;
            } else if (towerName == "Three") {
                Destroy (curPreview.gameObject);
                curPreview = null;
            }
        }
    }

    public void CloseHelp () {
        helpMenu.SetActive (false);
    }

    public void Help () {
        if (!SpawnManager.SM.victory || !SpawnManager.SM.defeat) {
            helpMenu.SetActive (true);
            upgradePanel.SetActive (false);
            buildPanel.SetActive (false);
            pauseMenu.SetActive (false);
        }
    }
}
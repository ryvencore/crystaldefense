﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseProjectile : MonoBehaviour {

    public Vector3 targetPosition;
    private float moveSpeed = 50f;
    private Vector3 moveDir;
    private int damageAmount;
    private BaseEnemy enemy;

    public static void Create (GameObject prefab, Vector3 spawnPosition, BaseEnemy enemy, int damageAmount) {
        Transform projectileT = Instantiate (prefab.transform, spawnPosition, Quaternion.identity);
        BaseProjectile projectile = projectileT.GetComponent<BaseProjectile> ();
        projectile.Init (enemy, damageAmount);
    }

    public void Init (BaseEnemy enemy, int damageAmount) {
        this.enemy = enemy;
        this.damageAmount = damageAmount;
    }
    public void Update () {
        if (enemy == null || enemy.IsDead ()) {
            Destroy (gameObject);
            return;
        }

        targetPosition = enemy.GetPosition ();
        moveDir = (targetPosition - transform.position).normalized;
        transform.position += moveDir * moveSpeed * Time.deltaTime;

        transform.rotation = Quaternion.LookRotation (targetPosition - transform.position);

        float destroyDist = 1f;
        if (Vector3.Distance (transform.position, targetPosition) < destroyDist) {
            enemy.Damage (damageAmount);
            Destroy (gameObject);
        }
    }

    public float GetAngleFromVectorFloat (Vector3 dir) {
        dir = dir.normalized;
        float x = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
        if (x < 0) x += 360;
        return x;
    }

}
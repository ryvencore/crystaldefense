﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SpawnManager : MonoBehaviour {

    public Transform spawnPosition;
    public List<Transform> waypointList = new List<Transform> ();
    public static SpawnManager SM;
    public Text timerText;
    public Text goldText;
    public Text roundText;
    public Text roundInfo;
    public float startTimer;
    public float spawnTime = 2f;
    public float spawnInterval = 1f;
    public float timer = 0f;
    public bool roundStarted = false;
    public int enemyAmountCur = 0;
    public int enemyAmountMax;
    public GameManager gm;
    public GameObject skipButton;
    public bool victory = false;
    public bool defeat = false;
    public GameObject victoryPanel;
    public GameObject defeatPanel;
    public List<BaseRound> rounds = new List<BaseRound> ();
    public Material[] mats;

    public void Awake () {
        SM = this;
        gm = GameManager.GM;
        victoryPanel.SetActive (false);
        defeatPanel.SetActive (false);
    }

    public void Update () {
        if (!victory || !defeat) {
            if (!roundStarted) {
                NewRound ();
            } else if (gm.round <= 12) {
                StartRound ();
            }
        }
        if (roundStarted) roundText.gameObject.SetActive (true);
        else roundText.gameObject.SetActive (false);

        if (!roundStarted) roundInfo.text = "Next: " + rounds[gm.round].rType.ToString ();
        else if (roundStarted) roundInfo.text = "Current: " + rounds[gm.round - 1].rType.ToString ();
        roundText.text = "Round: " + gm.round;
        goldText.text = "Gold: " + gm.gold;
    }

    public void SpawnEnemy () {
        BaseEnemy enemy = BaseEnemy.Create (rounds[gm.round - 1].enemyPrefab, spawnPosition.position);
        enemy.goldAmount = rounds[gm.round - 1].gold;
        enemy.health = rounds[gm.round - 1].health;
        enemy.speed = rounds[gm.round - 1].speed;
        enemy.unitName = rounds[gm.round - 1].unitName;
        enemy.enemyType = rounds[gm.round - 1].rType.ToString ();
        if (enemy.enemyType == "LIGHT") {
            enemy.gameObject.GetComponent<Renderer> ().material = mats[0];
        } else if (enemy.enemyType == "MEDIUM") {
            enemy.gameObject.GetComponent<Renderer> ().material = mats[1];
        } else if (enemy.enemyType == "HEAVY") {
            enemy.gameObject.GetComponent<Renderer> ().material = mats[2];
        }
    }

    //countdown to round start
    public void NewRound () {
        timerText.gameObject.SetActive (true);
        skipButton.SetActive (true);
        startTimer -= Time.deltaTime;
        int timerToInt = (int) startTimer;
        timerText.text = timerToInt.ToString ();
        if (startTimer <= 1) {
            startTimer = 16f;
            timerText.gameObject.SetActive (false);
            gm.round++;
            roundStarted = true;
        }
    }

    public void StartRound () {
        skipButton.SetActive (false);
        if (enemyAmountCur < enemyAmountMax) {
            timer += Time.deltaTime;
            if (timer > spawnTime) {
                SpawnEnemy ();
                enemyAmountCur++;
                timer = 0f;
            }
        } else if (enemyAmountCur == enemyAmountMax && BaseEnemy.enemyList.Count == 0) {
            enemyAmountCur = 0;
            if (gm.round <= 11) roundStarted = false;
            else Victory ();
        }
    }

    public void SkipToRound () {
        gm.round++;
        startTimer = 30f;
        timerText.gameObject.SetActive (false);
        roundStarted = true;
        skipButton.SetActive (false);
    }

    public void Victory () {
        roundText.gameObject.SetActive (false);
        victory = true;
        gm.round = 13;
        victoryPanel.SetActive (true);
    }

    public void Defeat () {
        roundText.gameObject.SetActive (false);
        defeat = true;
        defeatPanel.SetActive (true);
    }

    public void ToMainMenu () {
        SceneManager.LoadScene (0);
    }

    public void AppQuit () {
        Application.Quit ();
    }

    public void Restart () {
        SceneManager.LoadScene (2);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseEnemy : MonoBehaviour {

    public static List<BaseEnemy> enemyList = new List<BaseEnemy> ();
    public GameObject floatingText;
    private Vector3 floatingTextOffset = new Vector3 (0, 0, 0.5f);
    public HPManager healthSystem;
    public int health;
    public int goldAmount;
    public string unitName;
    public SpawnManager sm;
    public GameManager gm;
    public Transform nextWaypoint;
    public int wpIndex = 0;
    public Image hpFill;
    public int speed;
    public string enemyType;

    public interface IEnemyTargetable {
        Vector3 GetPosition ();
        void Damage (BaseEnemy attacker);
    }

    public void Start () {
        sm = SpawnManager.SM;
        gm = GameManager.GM;
        enemyList.Add (this);
        healthSystem = new HPManager (health);
        healthSystem.SetHealthMax (health, true);
        nextWaypoint = sm.waypointList[0];
    }

    public void Update () {
        Movement ();
        hpFill.fillAmount = ((float) healthSystem.health / (float) healthSystem.healthMax);

    }

    public static BaseEnemy Create (GameObject prefab, Vector3 position) {
        Transform enemyT = Instantiate (prefab.transform, position, Quaternion.identity);
        BaseEnemy enemyHandler = enemyT.GetComponent<BaseEnemy> ();
        return enemyHandler;
    }

    public static BaseEnemy GetClosestEnemy (Vector3 position, float maxRange) {
        BaseEnemy closest = null;
        foreach (BaseEnemy enemy in enemyList) {
            if (enemy.IsDead ()) {
                continue;
            }
            if (Vector3.Distance (position, enemy.GetPosition ()) <= maxRange) {
                if (closest == null) {
                    closest = enemy;
                } else {
                    if (Vector3.Distance (position, enemy.GetPosition ()) < Vector3.Distance (position, closest.GetPosition ())) {
                        closest = enemy;
                    }
                }
            }
        }
        return closest;
    }

    public void Damage (int damageAmount) {
        healthSystem.Damage (damageAmount);
        if (IsDead ()) {
            enemyList.Remove (this);
            CreateFloatingText (goldAmount);
            gm.gold += goldAmount;
            Destroy (gameObject);
        } else {
            transform.position += new Vector3 (Random.Range (-1f, 1f), 0f, Random.Range (-1f, 1f)).normalized * 0.15f;
        }
    }

    public void Damage (IEnemyTargetable attacker) {
        healthSystem.Damage (0);
        if (IsDead ()) {
            enemyList.Remove (this);
            CreateFloatingText (goldAmount);
            gm.gold += goldAmount;
            Destroy (gameObject);
        } else {
            transform.position += (GetPosition () - attacker.GetPosition ()).normalized * 0.15f;
        }
    }

    public void CreateFloatingText (int goldAmount) {
        GameObject clone = Instantiate (floatingText, GetPosition () + floatingTextOffset, Camera.main.transform.rotation);
        clone.GetComponent<TextMesh> ().text = "+" + goldAmount.ToString ();
        Destroy (clone, 1f);
    }

    public void Movement () {
        transform.position = Vector3.MoveTowards (transform.position, nextWaypoint.position, speed * Time.deltaTime);
    }

    private void OnTriggerEnter (Collider col) {
        if (col.tag == "WP") {
            if (wpIndex < sm.waypointList.Count - 1) {
                wpIndex++;
            }
            nextWaypoint = sm.waypointList[wpIndex];
        }
        if (col.tag == "Crystal") {
            gm.crystalLives -= 1;
            enemyList.Remove (this);
            Destroy (gameObject);
        }
    }

    public bool IsDead () {
        return healthSystem.IsDead ();
    }

    public Vector3 GetPosition () {
        return transform.position;
    }
}
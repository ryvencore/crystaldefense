﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BaseRound {
    public string unitName;
    public int health;
    public int gold;
    public int speed;
    public GameObject enemyPrefab;

    public enum RoundType {
        LIGHT,
        MEDIUM,
        HEAVY
    }

    public RoundType rType;
}
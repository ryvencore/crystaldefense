﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildSpot : MonoBehaviour {

    public GameObject buildPanel;
    public GameObject upgradePanel;
    public GameObject helpPanel;
    public GameObject pausePanel;

    public bool spotTaken = false;

    public void OnMouseDown () {
        if (!spotTaken && !SpawnManager.SM.victory) {
            if (!SpawnManager.SM.defeat) {
                BuildManager.BM.buildMenuOpen = true;
                buildPanel.SetActive (true);
                upgradePanel.SetActive (false);
                helpPanel.SetActive (false);
                pausePanel.SetActive (false);
                BuildManager.BM.curSelected = this.gameObject;
            }
        }
    }
}